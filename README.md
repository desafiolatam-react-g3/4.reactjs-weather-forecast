EJERCICIO 4: PRONÓSTICO DEL TIEMPO
---
Se debe desarrollar una aplicación que muestre el pronóstico del tiempo para una localidad dada.

La aplicación debe tener una barra de búsqueda en donde se ingresa el nombre de la localidad que se quiere conocer el pronóstico del tiempo.

La información desplegada, por localidad encontrada, debe mostrar el pronóstico del tiempo desde la hora actual hasta tres horas hacia adelante. Además se debe mostrar la información de los cinco días siguientes al actual. 

### Fuentas de datos
- Estación: `https://api.openweathermap.org/data/2.5/forecast?q={:location}&units=metric&appid={:app_id}`

## Recursos
- [Documentación oficial de Open Weather Map API](https://openweathermap.org/forecast5)
- [Documentación oficial de Reactjs](https://reactjs.org/docs)
- [Documentación oficial de Reduxjs](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
